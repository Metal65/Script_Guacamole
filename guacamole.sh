#! /bin/bash
reset

# variable de couleur 
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
ROUGE="\\033[1;31m"
CYAN="\\033[1;36m"

# Numéro de version de guacamole a installer.
GUACVERSION="0.9.14"


CLIGNOT="\\033[5mBlink"

# Signature

EFFECT_CLIGN="\033[3;30;31m"
ma_siganature ()
    {
echo -e   "$CYAN"     "|            ____  __           __   _   __             __       "$NORMAL"""|"
echo -e   "$CYAN"     "|           / __ )/ /___ ______/ /__/ | / /___  ____   / /_      "$NORMAL"""|"
echo -e   "$CYAN"     "|          / __  / / __  / ___/ //_/  |/ / __  / __  / __/ /     "$NORMAL"""|"
echo -e   "$CYAN"     "|         / /_/ / / /_/ / /__/ ,  / /|  / /_/ / /_/ / /_/ /      "$NORMAL"""|"
echo -e   "$CYAN"     "|        /_____/_/\__,_/\___/_/|_/_/ |_/\____/\____/_.___/       "$NORMAL"""|"
echo ""   ""            "|                               "V.1.1"                           "$NORMAL"""|"          
echo -e "$EFFECT_CLIGN"   "|        ---- Installation Script Guacamole V."$GUACVERSION" -----        "$NORMAL"""|"
$gu
    }

ma_siganature
echo ""
echo ""


# Effacement du depot DVD lié a un bug d'install sur Debian 
echo -e "$BLEU" "Effacement du deb DVD buggé pour installer les nouveaux paquets."
printf '\033[0m'
sed -i '/DVD/d' /etc/apt/sources.list
echo ""
echo -e "Terminé.."
echo ""
sleep 3

# Mettre à jour apt pour que nous puissions rechercher et installer la version la plus récente de tomcat et MariaDB supportée
echo -e "$ROUGE" "Mise a jour de apt-cache pour installer la dernière version de Tomcat sur Débian." "$NORMAL"
echo ""

printf '\033[0m'
apt-get update
echo -e "Terminé.."
sleep 5
reset

# Fonction de Tomcat8 et MariaDB

ma_siganature
echo ""
echo -e "$ROUGE" "Préparation et installation de Maria DB et Tomcat ! " "$NORMAL" 
sleep 3
echo ""
mariadb_tomcat ()   # début de la fonction
    {
        echo -e "$BLEU" "Installation en cours..."
        echo ""
        printf '\033[0m'
        mkdir -p /etc/guacamole/lib /etc/guacamole/extensions
        apt install tomcat8 mariadb-server -y
        
    }

# Installation de tomcat8 et Maria DB

mariadb_tomcat
echo -e "Terminé.."
reset


# Paramétrage MySQL
ma_siganature
echo ""
echo -e "$BLEU" "Obtenir le mot de passe root MySQL et le mot de passe utilisateur Guacamole" "$NORMAL" 
printf '\033[0m'
if [ -n "$mysqlpwd" ] && [ -n "$guacpwd" ]; then 
    mysqlrootpassword=$mysqlpwd 
    guacdbuserpassword=$guacpwd
else
    echo
    while true 
    do
        read -s -p "Entrer le mot de passe ROOT MySQL: " mysqlrootpassword
        echo
        read -s -p "Confirmer le mot de passe ROOT MySQL: " password2
        echo
        [ "$mysqlrootpassword" = "$password2" ] && break
        echo "Les mots de passes ne corresponde pas. Réessayer."
        echo
    done
    echo
    while true
    do
        read -s -p "Entrer le mot de passe utilisateur de la base de données Guacamole: " guacdbuserpassword
        echo
        read -s -p "Confirmer le mot de passe utilisateur de la base de données Guacamole: " password2
        echo
        [ "$guacdbuserpassword" = "$password2" ] && break
        echo "Les mots de passes ne corresponde pas. Réessayer."
        echo
    done
    echo
fi

debconf-set-selections <<< "mysql-server mysql-server/root_password password $mysqlrootpassword"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mysqlrootpassword"
reset

# Installation du composant et du pilote de la base de données Java (JDBC) pour MySQL / MariaDB, et liaison à la configuration de Guacamole:

echo -e "| "$ROUGE"  ---- Téléchargement des extensions d'authentification Guacamole. ---- "$NORMAL" ""|"
echo ""

printf '\033[0m'
apt install libmysql-java
ln -s /usr/share/java/mysql-connector-java.jar /etc/guacamole/lib/
reset

echo -e "$ROUGE" "Téléchargement du composant d'authentification JDBC Guacamole.Et copie du fichier à l'emplacement approprié. " "$NORMAL" 
echo ""

printf '\033[0m'
wget -c  https://sourceforge.net/projects/guacamole/files/current/extensions/guacamole-auth-jdbc-${GUACVERSION}.tar.gz
tar -xzvf guacamole-auth-jdbc-${GUACVERSION}.tar.gz
echo -e "$ROUGE" "Copie du fichier à l'emplacement approprié. " "$NORMAL" 
mv guacamole-auth-jdbc-${GUACVERSION}/mysql/guacamole-auth-jdbc-mysql-${GUACVERSION}.jar /etc/guacamole/extensions
sleep 1



# Installation des librairies  Guacamole serveur

reset
ma_siganature
echo ""
echo -e  "$ROUGE" "Installation des fonctionnalités Guacamole server depuis les sources. " "$NORMAL" 
sleep 1
echo ""
printf '\033[0m'
apt-get install libcairo2-dev libossp-uuid-dev libavcodec-dev libavutil-dev libswscale-dev libfreerdp-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev libjpeg62-turbo-dev libpng-dev libpng16-16 git -y
echo ""
reset

# Clonage des sources

ma_siganature
echo ""
echo -e "$ROUGE" "Clonage des sources... "
sleep 1
echo ""
printf '\033[0m'
git clone git://github.com/apache/incubator-guacamole-server.git

echo -e "$ROUGE" "Configuration et construction des sources... " ; sleep 1
printf '\033[0m'
cd incubator-guacamole-server
autoreconf -fi
./configure --with-init-dir=/etc/init.d
make && make install
ldconfig
systemctl enable guacd
/etc/init.d/guacd start
cd ..

# configuration de guacamole.properties

echo -e "$ROUGE" "Configuration du fichier guacamole.properties... "
printf '\033[0m'
touch /etc/guacamole/guacamole.properties

echo "guacd-hostname: localhost" >> /etc/guacamole/guacamole.properties
echo "guacd-port:     4822"      >> /etc/guacamole/guacamole.properties

echo "mysql-hostname: localhost" >> /etc/guacamole/guacamole.properties
echo "mysql-port: 3306" >> /etc/guacamole/guacamole.properties
echo "mysql-database: guacamole_db" >> /etc/guacamole/guacamole.properties
echo "mysql-username: guacamole_user" >> /etc/guacamole/guacamole.properties
echo "mysql-password: $guacdbuserpassword" >> /etc/guacamole/guacamole.properties
sleep 3
reset

echo "$ROUGE" "Liaison du fichier guacamole.properties à votre configuration Tomcat... " "$NORMAL"
ln -s /etc/guacamole/ /var/lib/tomcat8/.guacamole
sleep 2

# SQL code
SQLCODE="
create database guacamole_db;
create user 'guacamole_user'@'localhost' identified by \"$guacdbuserpassword\";
GRANT SELECT,INSERT,UPDATE,DELETE ON guacamole_db.* TO 'guacamole_user'@'localhost';
flush privileges;"

# Exexution du code SQL
echo $SQLCODE | mysql -u root -p$mysqlrootpassword

# Add Guacamole schema to newly created database
cat guacamole-auth-jdbc-${GUACVERSION}/mysql/schema/*.sql | mysql -u root -p$mysqlrootpassword guacamole_db


# Application web Guacamole
reset
ma_siganature
echo ""
echo -e  "$ROUGE" "Téléchargement de | l'application web Guacamole. " "$NORMAL"
printf '\033[0m'
echo ""
wget https://sourceforge.net/projects/guacamole/files/current/binary/guacamole-${GUACVERSION}.war
sleep 3

echo ""
echo -e "$ROUGE" "Copie sur Tomcat pour le déploiement et redémarrage de Tomcat "
mv guacamole-${GUACVERSION}.war /var/lib/tomcat8/webapps/guacamole.war

echo -e "$ROUGE" "Installation terminée...\nhttp://localhost:8080/guacamole/\nConnection par défault guacadmin:guacadmin\nAssurez-vous de changer le mot de passe."
echo ""



